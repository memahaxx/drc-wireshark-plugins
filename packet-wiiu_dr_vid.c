#include "config.h"
#include <epan/packet.h>
#define WIIU_DR_VID_DST_PORT 50120



static int proto_wiiu_dr_vid = -1;
static int hf_wiiu_dr_vid_h = -1;
static int hf_wiiu_dr_vid_h_magic = -1;
static int hf_wiiu_dr_vid_h_packet_type = -1;
static int hf_wiiu_dr_vid_h_seq_id = -1;
static int hf_wiiu_dr_vid_h_init = -1;
static int hf_wiiu_dr_vid_h_frame_begin = -1;
static int hf_wiiu_dr_vid_h_chunk_end = -1;
static int hf_wiiu_dr_vid_h_frame_end = -1;
static int hf_wiiu_dr_vid_h_has_timestamp = -1;
static int hf_wiiu_dr_vid_h_payload_size = -1;
static int hf_wiiu_dr_vid_timestamp = -1;

static gint ett_wiiu_dr_vid = -1;


static void
dissect_wiiu_dr_vid(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
	gint offset = 0;
	col_set_str(pinfo->cinfo, COL_PROTOCOL, "WIIU DR VID");
	/* Clear out stuff in the info column */
//	col_clear(pinfo->cinfo,COL_INFO);

	if (tree) { /* we are being asked for details */
		proto_item *ti = NULL;
		proto_tree *wiiu_dr_vid_tree = NULL;
		ti = proto_tree_add_item(tree, proto_wiiu_dr_vid, tvb, 0, -1, ENC_NA);
		wiiu_dr_vid_tree = proto_item_add_subtree(ti, ett_wiiu_dr_vid);
//		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h, tvb, offset, 4, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_magic, tvb, offset, 4, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_packet_type, tvb, offset, 4, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_seq_id, tvb, offset, 4, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_init, tvb, offset, 4, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_frame_begin, tvb, offset, 4, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_chunk_end, tvb, offset, 4, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_frame_end, tvb, offset, 4, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_has_timestamp, tvb, offset, 4, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_h_payload_size, tvb, offset, 4, ENC_BIG_ENDIAN);
		offset+=4;
		proto_tree_add_item(wiiu_dr_vid_tree, hf_wiiu_dr_vid_timestamp, tvb, offset, 4, ENC_BIG_ENDIAN);


		guint8 magic = tvb_get_guint8(tvb, 0) >>4;
		guint16 seq_id = tvb_get_ntohs(tvb, 0) & 0x3ff;
		guint8 flags = tvb_get_guint8(tvb, 2) >> 3;
		guint16 sz = tvb_get_ntohs(tvb, 2) & 0x7ff;
		guint32 ts = tvb_get_ntohl(tvb, 4);

		col_add_fstr(pinfo->cinfo,COL_INFO, "");
		col_append_fstr(pinfo->cinfo,COL_INFO, " TS: 0x%08x", ts);
		if(magic != 0xf) col_append_fstr(pinfo->cinfo,COL_INFO, " BAD MAGIC!");
		col_append_fstr(pinfo->cinfo,COL_INFO, " SEQ: 0x%04x", seq_id);
		col_append_fstr(pinfo->cinfo,COL_INFO, " %c%c%c%c%c",
			(flags & 0x10)?'I':' ',
			(flags & 0x08)?'B':' ',
			(flags & 0x04)?'C':' ',
			(flags & 0x02)?'E':' ',
			(flags & 0x01)?'T':' ');
		col_append_fstr(pinfo->cinfo,COL_INFO, " SZ: %5d", sz);

		int i=0;
		while(i<8)
		{
			guint8 ext = tvb_get_guint8(tvb, 8+i);
			switch(ext)
			{
				case 0x00:
				{
					i=8;
					break;
				}
				case 0x80:
				{
					col_append_fstr(pinfo->cinfo,COL_INFO, " IDR");
					i+=1;
					break;
				}
				case 0x81:
				{
					ext = tvb_get_guint8(tvb, 8+i+1);
					col_append_fstr(pinfo->cinfo,COL_INFO, " UNK_81: %02hhx", ext);
					i+=2;
					break;
				}
				case 0x82:
				{
					const char *fr[] =
					{
						"59.94Hz", "50Hz", "29.97Hz", "25Hz", "ERROR",
					};
					ext = tvb_get_guint8(tvb, 8+i+1);
					if(ext > 3) ext=4;
					col_append_fstr(pinfo->cinfo,COL_INFO, " Framerate: %s (%02hhx)", fr[ext], tvb_get_guint8(tvb, 8+i+1));
					i+=2;
					break;
				}
				case 0x83:
				{
					col_append_fstr(pinfo->cinfo,COL_INFO, " +FORCE_DEC");
					i+=1;
					break;
				}
				case 0x84:
				{
					col_append_fstr(pinfo->cinfo,COL_INFO, " -FORCE_DEC");
					i+=1;
					break;
				}
				case 0x85:
				{
					ext = tvb_get_guint8(tvb, 8+i+1);
					col_append_fstr(pinfo->cinfo,COL_INFO, " NUM_MB: %02hhx", ext);
					i+=2;
					break;
				}
				default:
				{
					col_append_fstr(pinfo->cinfo,COL_INFO, " FAIL");
					i=8;
				}
			}
		}
//*/
		
	}

}


void proto_register_wiiu_dr_vid(void)
{

	static hf_register_info hf[] = {
		{ &hf_wiiu_dr_vid_h_magic,
			{ "Magic", "wiiu_dr_vid.h.magic",
			FT_UINT32, BASE_HEX,
			NULL, 0xf0000000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_h_packet_type,
			{ "Packet type", "wiiu_dr_vid.h.packet_type",
			FT_UINT32, BASE_HEX,
			NULL, 0x0c000000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_h_seq_id,
			{ "Seqid", "wiiu_dr_vid.h.seq_id",
			FT_UINT32, BASE_HEX,
			NULL, 0x03ff0000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_h_init,
			{ "Init", "wiiu_dr_vid.h.init",
			FT_UINT32, BASE_HEX,
			NULL, 0x00008000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_h_frame_begin,
			{ "Frame begin", "wiiu_dr_vid.h.frame_begin",
			FT_UINT32, BASE_HEX,
			NULL, 0x00004000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_h_chunk_end,
			{ "Chunk end", "wiiu_dr_vid.h.chunk_end",
			FT_UINT32, BASE_HEX,
			NULL, 0x00002000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_h_frame_end,
			{ "Frame end", "wiiu_dr_vid.h.frame_end",
			FT_UINT32, BASE_HEX,
			NULL, 0x00001000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_h_has_timestamp,
			{ "Has timestamp", "wiiu_dr_vid.h.has_timestamp",
			FT_UINT32, BASE_HEX,
			NULL, 0x00000800,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_h_payload_size,
			{ "Payload size", "wiiu_dr_vid.h.payload_size",
			FT_UINT32, BASE_HEX,
			NULL, 0x000007ff,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_vid_timestamp,
			{ "Timestamp", "wiiu_dr_vid.timestamp",
			FT_UINT32, BASE_HEX,
			NULL, 0,
			NULL, HFILL }
		},
	};

	/* Setup protocol subtree array */
	static gint *ett[] = {
		&ett_wiiu_dr_vid
	};
	proto_wiiu_dr_vid = proto_register_protocol (
		"WiiU DR VID Protocol",
		"WiiU-DR-VID",
		"wiiu_dr_vid"
		);

	proto_register_field_array(proto_wiiu_dr_vid, hf, array_length(hf));
	proto_register_subtree_array(ett, array_length(ett));
}

void proto_reg_handoff_wiiu_dr_vid(void)
{
	static dissector_handle_t wiiu_dr_vid_handle;
	wiiu_dr_vid_handle = create_dissector_handle(dissect_wiiu_dr_vid, proto_wiiu_dr_vid);
	dissector_add_uint("udp.port", WIIU_DR_VID_DST_PORT, wiiu_dr_vid_handle);
}
