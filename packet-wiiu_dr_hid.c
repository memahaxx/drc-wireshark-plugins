#include "config.h"
#include <epan/packet.h>
#define WIIU_DR_HID_DST_PORT 50122



/*
#define HID_BUTTON_SYNC         0x000100
#define HID_BUTTON_HOME         0x000200
#define HID_BUTTON_MINUS        0x000400
#define HID_BUTTON_PLUS         0x000800
#define HID_BUTTON_R1           0x001000
#define HID_BUTTON_L1           0x002000
#define HID_BUTTON_R2           0x004000
#define HID_BUTTON_L2           0x008000
#define HID_BUTTON_DP_DOWN      0x000100
#define HID_BUTTON_DP_UP        0x000200
#define HID_BUTTON_DP_RIGHT     0x000400
#define HID_BUTTON_DP_LEFT      0x000800
#define HID_BUTTON_Y            0x001000
#define HID_BUTTON_X            0x002000
#define HID_BUTTON_B            0x004000
#define HID_BUTTON_A            0x008000

*/
#define HID_BUTTON_SYNC         0x0100
#define HID_BUTTON_HOME         0x0200
#define HID_BUTTON_MINUS        0x0400
#define HID_BUTTON_PLUS         0x0800
#define HID_BUTTON_R1           0x1000
#define HID_BUTTON_L1           0x2000
#define HID_BUTTON_R2           0x4000
#define HID_BUTTON_L2           0x8000
#define HID_BUTTON_DP_DOWN      0x0001
#define HID_BUTTON_DP_UP        0x0002
#define HID_BUTTON_DP_RIGHT     0x0004
#define HID_BUTTON_DP_LEFT      0x0008
#define HID_BUTTON_Y            0x0010
#define HID_BUTTON_X            0x0020
#define HID_BUTTON_B            0x0040
#define HID_BUTTON_A            0x0080
#define POWER_PLUGGED_IN    0x01
#define POWER_BUTTON_PRESS  0x02
#define POWER_CHARGING      0x40

static int proto_wiiu_dr_hid = -1;
static int hf_wiiu_dr_hid_firmware_version = -1;
static int hf_wiiu_dr_hid_sequence_number = -1;
static int hf_wiiu_dr_hid_buttons = -1;
static int hf_wiiu_dr_hid_buttons_sync = -1;
static int hf_wiiu_dr_hid_buttons_home = -1;
static int hf_wiiu_dr_hid_buttons_minus = -1;
static int hf_wiiu_dr_hid_buttons_plus = -1;
static int hf_wiiu_dr_hid_buttons_r1 = -1;
static int hf_wiiu_dr_hid_buttons_l1 = -1;
static int hf_wiiu_dr_hid_buttons_r2 = -1;
static int hf_wiiu_dr_hid_buttons_l2 = -1;
static int hf_wiiu_dr_hid_buttons_dp_down = -1;
static int hf_wiiu_dr_hid_buttons_dp_up = -1;
static int hf_wiiu_dr_hid_buttons_dp_right = -1;
static int hf_wiiu_dr_hid_buttons_dp_left = -1;
static int hf_wiiu_dr_hid_buttons_y = -1;
static int hf_wiiu_dr_hid_buttons_x = -1;
static int hf_wiiu_dr_hid_buttons_b = -1;
static int hf_wiiu_dr_hid_buttons_a = -1;

static int hf_wiiu_dr_hid_power = -1;
static int hf_wiiu_dr_hid_power_plugged_in = -1;
static int hf_wiiu_dr_hid_power_button = -1;
static int hf_wiiu_dr_hid_power_charging = -1;

static int hf_wiiu_dr_hid_charge = -1;

static int hf_wiiu_dr_hid_waiting_for_streaming = -1;



static gint ett_wiiu_dr_hid = -1;




static void
dissect_wiiu_dr_hid(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
	gint offset = 0;
	col_set_str(pinfo->cinfo, COL_PROTOCOL, "WIIU DR HID");
	/* Clear out stuff in the info column */
	col_clear(pinfo->cinfo,COL_INFO);

	if (tree) { /* we are being asked for details */
		proto_item *ti = NULL;
		proto_tree *wiiu_dr_hid_tree = NULL;
		ti = proto_tree_add_item(tree, proto_wiiu_dr_hid, tvb, 0, -1, ENC_NA);
		wiiu_dr_hid_tree = proto_item_add_subtree(ti, ett_wiiu_dr_hid);
//		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_firmware_version, tvb, offset, 1, ENC_LITTLE_ENDIAN);
//		offset+=1;
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_sequence_number, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		offset+=2;
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_sync, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_home, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_minus, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_plus, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_r1, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_l1, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_r2, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_l2, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_dp_down, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_dp_up, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_dp_right, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_dp_left, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_y, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_x, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_b, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_buttons_a, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		offset+=2;
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_power, tvb, offset, 1, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_power_plugged_in, tvb, offset, 1, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_power_button, tvb, offset, 1, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_power_charging, tvb, offset, 1, ENC_LITTLE_ENDIAN);
		offset+=1;
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_charge, tvb, offset, 1, ENC_LITTLE_ENDIAN);
		offset+=1;

		offset=83;
		proto_tree_add_item(wiiu_dr_hid_tree, hf_wiiu_dr_hid_waiting_for_streaming, tvb, offset, 1, ENC_LITTLE_ENDIAN);
	}
}


void proto_register_wiiu_dr_hid(void)
{

	static hf_register_info hf[] = {
		{ &hf_wiiu_dr_hid_sequence_number,
			{ "Sequence Number", "wiiu_dr_hid.seq",
			FT_UINT16, BASE_DEC,
			NULL, 0x0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons,
			{ "Buttons", "wiiu_dr_hid.buttons",
			FT_UINT16, BASE_HEX,
			NULL, 0x0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_sync,
			{ "Sync", "wiiu_dr_hid.buttons.sync",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_SYNC,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_home,
			{ "Home", "wiiu_dr_hid.buttons.home",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_HOME,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_minus,
			{ "Minus", "wiiu_dr_hid.buttons.minus",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_MINUS,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_plus,
			{ "Plus", "wiiu_dr_hid.buttons.plus",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_PLUS,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_r1,
			{ "R1", "wiiu_dr_hid.buttons.r1",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_R1,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_l1,
			{ "L1", "wiiu_dr_hid.buttons.l1",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_L1,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_r2,
			{ "R2", "wiiu_dr_hid.buttons.r2",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_R2,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_l2,
			{ "L2", "wiiu_dr_hid.buttons.l2",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_L2,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_dp_down,
			{ "DP Down", "wiiu_dr_hid.buttons.dp_down",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_DP_DOWN,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_dp_up,
			{ "DP Up", "wiiu_dr_hid.buttons.dp_up",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_DP_UP,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_dp_right,
			{ "DP Right", "wiiu_dr_hid.buttons.dp_right",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_DP_RIGHT,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_dp_left,
			{ "DP Up", "wiiu_dr_hid.buttons.dp_left",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_DP_LEFT,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_y,
			{ "Y", "wiiu_dr_hid.buttons.y",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_Y,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_x,
			{ "X", "wiiu_dr_hid.buttons.x",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_X,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_b,
			{ "B", "wiiu_dr_hid.buttons.b",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_B,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_buttons_a,
			{ "A", "wiiu_dr_hid.buttons.a",
			FT_BOOLEAN, 16,
			NULL, HID_BUTTON_A,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_power,
			{ "Power", "wiiu_dr_hid.power",
			FT_UINT8, BASE_HEX,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_power_plugged_in,
			{ "Plugged in", "wiiu_dr_hid.power.plugged_in",
			FT_BOOLEAN, 8,
			NULL, POWER_PLUGGED_IN,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_power_button,
			{ "Power button", "wiiu_dr_hid.power.button",
			FT_BOOLEAN, 8,
			NULL, POWER_BUTTON_PRESS,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_power_charging,
			{ "Charging", "wiiu_dr_hid.power.charging",
			FT_BOOLEAN, 8,
			NULL, POWER_CHARGING,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_charge,
			{ "Battery charge", "wiiu_dr_hid.power.charge",
			FT_UINT8, BASE_HEX,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_hid_waiting_for_streaming,
			{ "Waiting for streaming", "wiiu_dr_hid.waiting_for_streaming",
			FT_BOOLEAN, 8,
			NULL, 1,
			NULL, HFILL }
		},
	};

	/* Setup protocol subtree array */
	static gint *ett[] = {
		&ett_wiiu_dr_hid
	};
	proto_wiiu_dr_hid = proto_register_protocol (
		"WiiU DR HID Protocol",
		"WiiU-DR-HID",
		"wiiu_dr_hid"
		);

	proto_register_field_array(proto_wiiu_dr_hid, hf, array_length(hf));
	proto_register_subtree_array(ett, array_length(ett));
}

void proto_reg_handoff_wiiu_dr_hid(void)
{
	static dissector_handle_t wiiu_dr_hid_handle;
	wiiu_dr_hid_handle = create_dissector_handle(dissect_wiiu_dr_hid, proto_wiiu_dr_hid);
	dissector_add_uint("udp.port", WIIU_DR_HID_DST_PORT, wiiu_dr_hid_handle);
}
