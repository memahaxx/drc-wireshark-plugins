#include "config.h"
#include <epan/packet.h>
#define WIIU_DR_CMD_DST_PORT 50123



static int proto_wiiu_dr_cmd = -1;
static int hf_wiiu_dr_cmd_h_ptype = -1;
static int hf_wiiu_dr_cmd_h_qtype = -1;
static int hf_wiiu_dr_cmd_h_plsize = -1;
static int hf_wiiu_dr_cmd_h_seqid = -1;
static int hf_wiiu_dr_cmd_q0_pri = -1;
static int hf_wiiu_dr_cmd_q0_sec = -1;
static int hf_wiiu_dr_cmd_q0_pl = -1;

static gint ett_wiiu_dr_cmd = -1;


const char *ptypes[] =
{
	"Q ", "QA", "R ", "RA", "ER"
};

static void
dissect_wiiu_dr_cmd(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
	gint offset = 0;
	col_set_str(pinfo->cinfo, COL_PROTOCOL, "WIIU DR CMD");
	/* Clear out stuff in the info column */
//	col_clear(pinfo->cinfo,COL_INFO);

	if (tree) { /* we are being asked for details */
		proto_item *ti = NULL;
		proto_tree *wiiu_dr_cmd_tree = NULL;
		ti = proto_tree_add_item(tree, proto_wiiu_dr_cmd, tvb, 0, -1, ENC_NA);
		wiiu_dr_cmd_tree = proto_item_add_subtree(ti, ett_wiiu_dr_cmd);
		proto_tree_add_item(wiiu_dr_cmd_tree, hf_wiiu_dr_cmd_h_ptype, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		offset+=2;
		proto_tree_add_item(wiiu_dr_cmd_tree, hf_wiiu_dr_cmd_h_qtype, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		offset+=2;
		proto_tree_add_item(wiiu_dr_cmd_tree, hf_wiiu_dr_cmd_h_plsize, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		offset+=2;
		proto_tree_add_item(wiiu_dr_cmd_tree, hf_wiiu_dr_cmd_h_seqid, tvb, offset, 2, ENC_LITTLE_ENDIAN);
		offset+=2;
		guint16 ptype_index = tvb_get_letohs(tvb, 0);
		guint16 qtype_index = tvb_get_letohs(tvb, 2);
		if(ptype_index > 3) ptype_index = 4;	

		col_add_fstr(pinfo->cinfo,COL_INFO, "%s: %d",
			ptypes[ptype_index],
			tvb_get_letohs(tvb, 2)
		);

		if(qtype_index == 0 && (ptype_index == 0 || ptype_index == 2))
		{
			offset+=6;
			proto_tree_add_item(wiiu_dr_cmd_tree, hf_wiiu_dr_cmd_q0_pri, tvb, offset, 1, ENC_LITTLE_ENDIAN);
			offset++;
			proto_tree_add_item(wiiu_dr_cmd_tree, hf_wiiu_dr_cmd_q0_sec, tvb, offset, 1, ENC_LITTLE_ENDIAN);
			offset++;
			offset +=2;
			proto_tree_add_item(wiiu_dr_cmd_tree, hf_wiiu_dr_cmd_q0_pl, tvb, offset, 2, ENC_BIG_ENDIAN);
			offset++;

			guint8 magic = tvb_get_guint8(tvb, 8);
			if(magic != 0x7e)
			{
				col_append_fstr(pinfo->cinfo,COL_INFO, " BAD MAGIC!");
			}
			guint8 pri_id = tvb_get_guint8(tvb, 14);
			guint8 sec_id = tvb_get_guint8(tvb, 15);
			col_append_fstr(pinfo->cinfo,COL_INFO, " id: %hhd:%hhd", pri_id, sec_id);

			guint16 pl_size = tvb_get_ntohs(tvb, 18);
			col_append_fstr(pinfo->cinfo,COL_INFO, " pl_size: %hd", pl_size);
		}
	}

}


void proto_register_wiiu_dr_cmd(void)
{

	static hf_register_info hf[] = {
		{ &hf_wiiu_dr_cmd_h_ptype,
			{ "Packet type", "wiiu_dr_cmd.ptype",
			FT_UINT16, BASE_DEC,
			NULL, 0x0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_cmd_h_qtype,
			{ "Query type", "wiiu_dr_cmd.qtype",
			FT_UINT16, BASE_DEC,
			NULL, 0x0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_cmd_h_plsize,
			{ "Payload size", "wiiu_dr_cmd.plsize",
			FT_UINT16, BASE_DEC,
			NULL, 0x0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_cmd_h_seqid,
			{ "Sequence", "wiiu_dr_cmd.seqid",
			FT_UINT16, BASE_DEC,
			NULL, 0x0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_cmd_q0_pri,
			{ "Q0 Primary Id:", "wiiu_dr_cmd.q0pri",
			FT_UINT8, BASE_DEC,
			NULL, 0x0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_cmd_q0_sec,
			{ "Q0 Secondary Id:", "wiiu_dr_cmd.q0sec",
			FT_UINT8, BASE_DEC,
			NULL, 0x0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_cmd_q0_pl,
			{ "Q0 Payload:", "wiiu_dr_cmd.q0pl",
			FT_UINT16, BASE_DEC,
			NULL, 0x0,
			NULL, HFILL }
		},
	};

	/* Setup protocol subtree array */
	static gint *ett[] = {
		&ett_wiiu_dr_cmd
	};
	proto_wiiu_dr_cmd = proto_register_protocol (
		"WiiU DR CMD Protocol",
		"WiiU-DR-CMD",
		"wiiu_dr_cmd"
		);

	proto_register_field_array(proto_wiiu_dr_cmd, hf, array_length(hf));
	proto_register_subtree_array(ett, array_length(ett));
}

void proto_reg_handoff_wiiu_dr_cmd(void)
{
	static dissector_handle_t wiiu_dr_cmd_handle;
	wiiu_dr_cmd_handle = create_dissector_handle(dissect_wiiu_dr_cmd, proto_wiiu_dr_cmd);
	dissector_add_uint("udp.port", WIIU_DR_CMD_DST_PORT, wiiu_dr_cmd_handle);
}
