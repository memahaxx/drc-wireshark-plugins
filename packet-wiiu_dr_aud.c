#include "config.h"
#include <epan/packet.h>
#define WIIU_DR_AUD_DST_PORT 50121



static int proto_wiiu_dr_aud = -1;
static int hf_wiiu_dr_aud_h_format = -1;
static int hf_wiiu_dr_aud_h_channel = -1;
static int hf_wiiu_dr_aud_h_vibrate = -1;
static int hf_wiiu_dr_aud_h_type = -1;
static int hf_wiiu_dr_aud_h_seq_id = -1;
static int hf_wiiu_dr_aud_h_payload_size = -1;
static int hf_wiiu_dr_aud_h_timestamp = -1;

static int hf_wiiu_dr_aud_msg_timestamp = -1;
static int hf_wiiu_dr_aud_msg_freq0_0 = -1;
static int hf_wiiu_dr_aud_msg_freq0_1 = -1;
static int hf_wiiu_dr_aud_msg_freq1_0 = -1;
static int hf_wiiu_dr_aud_msg_freq1_1 = -1;
static int hf_wiiu_dr_aud_msg_vidformat = -1;

static gint ett_wiiu_dr_aud = -1;


static void
dissect_wiiu_dr_aud(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
	gint offset = 0;
	col_set_str(pinfo->cinfo, COL_PROTOCOL, "WIIU DR AUD");
	/* Clear out stuff in the info column */
//	col_clear(pinfo->cinfo,COL_INFO);

	if (tree) { /* we are being asked for details */
		proto_item *ti = NULL;
		proto_tree *wiiu_dr_aud_tree = NULL;
		ti = proto_tree_add_item(tree, proto_wiiu_dr_aud, tvb, 0, -1, ENC_NA);
		wiiu_dr_aud_tree = proto_item_add_subtree(ti, ett_wiiu_dr_aud);
//		proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_h, tvb, offset, 4, ENC_LITTLE_ENDIAN);
		proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_h_format, tvb, offset, 2, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_h_channel, tvb, offset, 2, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_h_vibrate, tvb, offset, 2, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_h_type, tvb, offset, 2, ENC_BIG_ENDIAN);
		proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_h_seq_id, tvb, offset, 2, ENC_BIG_ENDIAN);
		offset+=2;
		proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_h_payload_size, tvb, offset, 2, ENC_BIG_ENDIAN);
		offset+=2;
		proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_h_timestamp, tvb, offset, 4, ENC_LITTLE_ENDIAN);
		offset+=4;

		guint32 ts = tvb_get_letohl(tvb, 4);
		guint16 seq_id = tvb_get_ntohs(tvb, 0) & 0x3ff;
		guint8 type = (tvb_get_guint8(tvb, 0)>>2)&1;

		col_add_fstr(pinfo->cinfo,COL_INFO, "");
		col_append_fstr(pinfo->cinfo,COL_INFO, " TS: 0x%08x", ts);
		col_append_fstr(pinfo->cinfo,COL_INFO, " SEQ: 0x%04x", seq_id);
		col_append_fstr(pinfo->cinfo,COL_INFO, " TYPE: 0x%02x", type);

		if(type)
		{
			proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_msg_timestamp, tvb, offset, 4, ENC_LITTLE_ENDIAN);
			ts = tvb_get_letohl(tvb, offset);
			col_append_fstr(pinfo->cinfo,COL_INFO, " M_TS: 0x%08x", ts);
			offset+=4;

			col_append_fstr(pinfo->cinfo,COL_INFO, " F:", ts);
			
			guint32 f;
			proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_msg_freq0_0, tvb, offset, 4, ENC_LITTLE_ENDIAN);
			f = tvb_get_letohl(tvb, offset);
			col_append_fstr(pinfo->cinfo,COL_INFO, " (%d,", f);
			offset+=4;
			proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_msg_freq0_1, tvb, offset, 4, ENC_LITTLE_ENDIAN);
			f = tvb_get_letohl(tvb, offset);
			col_append_fstr(pinfo->cinfo,COL_INFO, " %d),", f);
			offset+=4;
			proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_msg_freq1_0, tvb, offset, 4, ENC_LITTLE_ENDIAN);
			f = tvb_get_letohl(tvb, offset);
			col_append_fstr(pinfo->cinfo,COL_INFO, " (%d,", f);
			offset+=4;
			proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_msg_freq1_1, tvb, offset, 4, ENC_LITTLE_ENDIAN);
			f = tvb_get_letohl(tvb, offset);
			col_append_fstr(pinfo->cinfo,COL_INFO, " %d)", f);
			offset+=4;
			proto_tree_add_item(wiiu_dr_aud_tree, hf_wiiu_dr_aud_msg_vidformat, tvb, offset, 4, ENC_LITTLE_ENDIAN);
			f = tvb_get_letohl(tvb, offset);
			col_append_fstr(pinfo->cinfo,COL_INFO, " VF: %d", f);
		}
//*/
		
	}

}


void proto_register_wiiu_dr_aud(void)
{

	static hf_register_info hf[] = {
		{ &hf_wiiu_dr_aud_h_format,
			{ "Format", "wiiu_dr_aud.h.format",
			FT_UINT16, BASE_HEX,
			NULL, 0xe000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_h_channel,
			{ "Channel", "wiiu_dr_aud.h.channel",
			FT_UINT16, BASE_HEX,
			NULL, 0x1000,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_h_vibrate,
			{ "Vibrate", "wiiu_dr_aud.h.vibrate",
			FT_UINT16, BASE_HEX,
			NULL, 0x0800,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_h_type,
			{ "Type", "wiiu_dr_aud.h.type",
			FT_UINT16, BASE_HEX,
			NULL, 0x0400,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_h_seq_id,
			{ "Seqid", "wiiu_dr_aud.h.seq_id",
			FT_UINT16, BASE_HEX,
			NULL, 0x03ff,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_h_payload_size,
			{ "Payload size", "wiiu_dr_aud.h.payload_size",
			FT_UINT16, BASE_HEX,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_h_timestamp,
			{ "Timestamp", "wiiu_dr_aud.h.timestamp",
			FT_UINT32, BASE_HEX,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_msg_timestamp,
			{ "MSG timestamp", "wiiu_dr_aud.msg.timestamp",
			FT_UINT32, BASE_HEX,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_msg_freq0_0,
			{ "Freq0_0", "wiiu_dr_aud.msg.freq0_0",
			FT_UINT32, BASE_DEC,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_msg_freq0_1,
			{ "Freq0_1", "wiiu_dr_aud.msg.freq0_1",
			FT_UINT32, BASE_DEC,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_msg_freq1_0,
			{ "Freq1_0", "wiiu_dr_aud.msg.freq1_0",
			FT_UINT32, BASE_DEC,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_msg_freq1_1,
			{ "Freq1_1", "wiiu_dr_aud.msg.freq1_1",
			FT_UINT32, BASE_DEC,
			NULL, 0,
			NULL, HFILL }
		},
		{ &hf_wiiu_dr_aud_msg_vidformat,
			{ "Vidformat", "wiiu_dr_aud.msg.vidformat",
			FT_UINT8, BASE_DEC,
			NULL, 0,
			NULL, HFILL }
		},
	};

	/* Setup protocol subtree array */
	static gint *ett[] = {
		&ett_wiiu_dr_aud
	};
	proto_wiiu_dr_aud = proto_register_protocol (
		"WiiU DR AUD Protocol",
		"WiiU-DR-AUD",
		"wiiu_dr_aud"
		);

	proto_register_field_array(proto_wiiu_dr_aud, hf, array_length(hf));
	proto_register_subtree_array(ett, array_length(ett));
}

void proto_reg_handoff_wiiu_dr_aud(void)
{
	static dissector_handle_t wiiu_dr_aud_handle;
	wiiu_dr_aud_handle = create_dissector_handle(dissect_wiiu_dr_aud, proto_wiiu_dr_aud);
	dissector_add_uint("udp.port", WIIU_DR_AUD_DST_PORT, wiiu_dr_aud_handle);
}
